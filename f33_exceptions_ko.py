#!/usr/bin/python3
""" ce script teste les exceptions """

def mon_exception(valeur1, valeur2):
    """ gestion des exceptions """
    resultat = valeur1 / valeur2
    print("Resultat=" + str(resultat))

# main
if __name__ == "__main__":
    mon_exception(6, 2)
    mon_exception(5, 0)

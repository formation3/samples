#!/usr/bin/python3
""" ce script affiche les parametres sur la console """

def affiche_valeur():
    """ Fonction qui affiche la variable """
    a = 8
    print("valeur dans la fonction = " + str(a))

def affiche_valeur2():
    """ Fonction qui affiche la variable """
    global a
    a = 9

# main 
a = 5
affiche_valeur()
print("valeur en global = " + str(a))

affiche_valeur2()
print("valeur en global = " + str(a))

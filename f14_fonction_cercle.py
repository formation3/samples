#!/usr/bin/python3
""" aire et perimetre d'un cercle """
import math

def calcul(le_rayon):
    """ renvoi le perimetre et l'aire """
    le_perimetre = 2 * le_rayon * math.pi
    l_aire = math.pi * le_rayon * le_rayon
    return le_perimetre, l_aire

if __name__ == "__main__":
    PERIMETRE, AIRE = calcul(3)
    print("Perimetre=" + str(PERIMETRE))
    print("Aire=" + str(AIRE))

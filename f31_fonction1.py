#!/usr/bin/python3
""" ce script test la fonction IF """
import random

def lance_jeu():
    """ Jeu de plus petit, plus grand. Nombre de 1 a 1000 """
    essai = 0
    # definir un nombre aleatoire
    resultat = random.randint(1, 1000)
    rejouer = True
    while rejouer:
        essai += 1
        ma_reponse = int(input("Saisissez une valeur>"))
        if ma_reponse > resultat:
            print("Plus petit")
        elif ma_reponse < resultat:
            print("Plus grand")
        else:
            # victoire
            print("Vous avez GAGNE en " + str(essai) + " coups !")
            rejouer = False

if __name__ == "__main__":
    lance_jeu()
